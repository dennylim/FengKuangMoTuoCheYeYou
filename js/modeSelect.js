/**
 * Created by Administrator on 2016/3/15.
 */
function ModeSelect(){
    var self = this;
    //外面盒子相当于body
    self.$outbox = $("<div class='outbox'></div>");
    var $top = $("<div></div>");
    $top.css({
        width:"100%",height:"80%","background-image":"url(imgs/modeselect_bg.jpg)",
        position:"absolute",
        "background-repeat":"no-repeat",
        "background-size":"cover",
        "backgrond-position":"bottom",
        "overflow":"hidden",
        "min-width":"1136px",
        "min-height":"535px",
        top:"0"
    });
    $top.appendTo(self.$outbox);

    //帮助按钮
    var $help = $("<div></div>");
    $help.css({
        position: "absolute",
        top: "5%",
        left:"180px",
        cursor: "pointer",
        width:"95px",
        height:"95px",
        "background-image":"url(imgs/help.png)"
    });
    $help.appendTo($top);
    //返回按钮
    var $return = $("<div id='return'></div>");
    $return.css({
        position: "absolute",
        top: "5%",
        right:"100px",
        cursor: "pointer",
        width:"95px",
        height:"95px",
        "background-image":"url(imgs/return.png)"
    });
    $return.appendTo($top);
    //点击返回按钮跳转到登录界面
    $return.click(function(){
        director.runSence(new LoginSence());
    });
    //var $rider = $("<div></div>");
    //$rider.css({
    //    position: "absolute",
    //    top: "15%",
    //    left:"180px",
    //    cursor: "pointer",
    //    width:"284px",
    //    height:"383px",
    //    "background-image":"url(imgs/rider1.png)"
    //});
    //$rider.appendTo(self.$outbox);
    //var $moto = $("<div></div>");
    //$moto.css({
    //    position: "absolute",
    //    top: "44%",
    //    left:"40%",
    //    cursor: "pointer",
    //    width:"365px",
    //    height:"236px",
    //    "background-image":"url(imgs/moto1.png)"
    //});
    //$moto.appendTo(self.$outbox);
    //底部
    var $bottom = $("<div></div>");
    $bottom.css({
        "background-color":"black",
        width:"100%",
        height:"17%",
        position:"absolute",
        bottom:0
    });
    $bottom.appendTo(self.$outbox);
    //底部盒子
    var $botbox=$("<div></div>");
    $botbox.css({
        width:"900px",
        height:"69px",
        position:"absolute",
        left:"calc(50% - 450px)",
        top:"calc(50% - 34.5px)"
    });
    $botbox.appendTo($bottom);
    //单人游戏按钮
    var $single = $("<div></div>");
    $single.css({
        width:"263px",
        height:"69px",
        //position:"absolute",
        //bottom:"5%",
        //left:"20%",
        float:"left",
        "background-image": "url(imgs/singleplayer.png)",
        cursor:"pointer"
    });
    $single.appendTo($botbox);
    //点击单人游戏进入地图选择界面
    $single.click(function(){
        director.runSence(new MapBanner());
    });
    //个人车库按钮
    var $persongarage = $single.clone();
    $persongarage.css({
        "background-image": "url(imgs/shop/mygarage.png)","margin-left":"55px","background-size":"263px 69px"
    });
    $persongarage.appendTo($botbox);
    //点击个人车库按钮进入个人车库界面
    $persongarage.click(function(){
        director.runSence(new GarageSence());
    });
//        var $pergaragetext =$("<span></span>");
//        $pergaragetext.css({
//            width:"128px", height:'35px',
//            "font-size":"32px","font-family":"微软雅黑","text-align":"center","text-fill-color":"#fef93c","text-strock-color":"black",
//            "text-stroke-width":"2px","font-weight":"bolder",left:"67px",position:"absolute",top:"10px"
//            "background-image": "url(imgs/mygarage.png)"
//        });
//        $pergaragetext.html("个人仓库");
//        $pergaragetext.appendTo($persongarage);
    //商店按钮
    var $shop = $single.clone();
    $shop.css({
        "background-image": "url(imgs/pattenChshop.png)","margin-left":"55px"
    });
    $shop.appendTo($botbox);
    $shop.click(function(){
        director.runSence(new ShopSence());
    })

}