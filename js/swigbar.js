/**
 * Created by Administrator on 2016/3/2.
 */
function SwigBar($parent){
    //将函数中用到的所有数字常量设置为变量，达到一改全改的目的
    var BAR_SIZE = 200;
    var BAR_HSIZE = 100;
    var LCIR_R = 100;    //大圆半径
    var SCIR_R = 30;    //小圆半径
    var SC_BG = "rgba(100, 100, 100, 0.5)";
    var W_BL = BAR_SIZE/$parent.width();//本控件和父元素宽之比
    var H_BL = BAR_SIZE/$parent.height();//本控件和高元素的高之比
    var self = this;
    var dx, dy;
    //创建画布
    this.$swig=$("<div></div>");
    this.$swig.css({
        width: "200px",
        height: "200px",
        position: "absolute",
        left: "50px",
        bottom: "20px",
        "z-index": "1000"
    });
    this.$swig.appendTo($parent);
    this.$bar = $("<canvas width="+BAR_SIZE+" height="+BAR_SIZE+"></canvas>");
    this.$bar.css({width:"100%", height: "100%","z-index": "1000"});                            //画布样式
    this.$bar.appendTo(this.$swig);                                              //添加到父节点中
    this.draw = function(ex, ey){                                           //绘画函数
        var ctx = self.$bar[0].getContext("2d");                            //
        ctx.clearRect(0, 0, BAR_SIZE, BAR_SIZE);                            //清除画布
        // 画外部大圆
        ctx.beginPath();
        ctx.fillStyle="#999";
        ctx.arc(BAR_HSIZE, BAR_HSIZE, LCIR_R, 0, 2*Math.PI,true);   //2*Math.PI=2π （弧度制）
        ctx.closePath();
        ctx.strokeStyle = "white";
        ctx.lineWidth = 3;
        ctx.stroke();
        //画内部的小圆
        ctx.beginPath();
        ctx.fillStyle="#666";
        if(ex!=undefined){    //判断是否传入参数，如果没传参
            var lr = Math.sqrt(Math.pow(ex-BAR_HSIZE, 2)+Math.pow(ey-BAR_HSIZE, 2));//鼠标离圆心距离（两点间距离公式）
            //console.log(ey);
            if(lr<LCIR_R-SCIR_R){
                dx=ex-BAR_HSIZE;
                dy=ey-BAR_HSIZE;
                ctx.arc(ex, ey, SCIR_R, 0, 2*Math.PI,true);
            }else {
                var xx = ex - BAR_HSIZE;//鼠标x和圆心X的差
                var yy = ey - BAR_HSIZE;//鼠标y和圆心y的差
                dx=xx*(LCIR_R-SCIR_R)/lr;//小圆现在所处x和圆心x的差
                dy=yy*(LCIR_R-SCIR_R)/lr;//小圆现在所处y和圆心y的差
                ctx.arc(BAR_HSIZE + dx, BAR_HSIZE + dy, SCIR_R, 0, 2 * Math.PI,true);
            }
        }
        else{
            ctx.arc(BAR_HSIZE, BAR_HSIZE, SCIR_R, 0, 2*Math.PI,true);
        }
        ctx.closePath();
//        ctx.fillStyle = SC_BG;
        ctx.fill();
        ctx.stroke();
    };

    this.draw();

    var dragging = false;
    var $me = $("<p></p>");
    this.$bar.mousedown(function(e){
        //开始拖拽
        dragging = true;
        var ex = e.clientX-self.$bar.offset().left;
        var ey = e.clientY-self.$bar.offset().top;
        self.draw(ex*W_BL, ey*H_BL);
    });
    $(document).mousemove(function(e){          //document(文档)
        if(dragging){
            //圆心位置=鼠标位置-父元素div的偏移值
            var ex = e.clientX-self.$bar.offset().left;
            var ey = e.clientY-self.$bar.offset().top;
            self.draw(ex*W_BL, ey*H_BL);
            console.log(self.$bar.offset().left, e.clientX);
            $me.trigger("rock",
                 [dx/(LCIR_R-SCIR_R), dy/(LCIR_R-SCIR_R)]);

            self.$bar.css({cousor:"move"});
    }
        e.preventDefault();
    });
    $(document).mouseup(function(e){
        dragging = false;
        self.draw();
        $me.trigger("rock",[0,0]);
        e.preventDefault();
        self.$bar.css({cousor:"default"});
    });

    this.bind = function(evt, fu){
        $me.on(evt, fu);

    }
}


