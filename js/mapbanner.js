function MapBanner(){
	//this指banner
	var self = this;
	//左右翻页控件
	//左翻页
	self.$outbox = $("<div></div>");
	self.$outbox.css({
		height: "100%",
		width: "100%",
		backgroundImage: "url(imgs/login_bg.jpg)",
		backgroundPosition:" center",
		"background-size": "cover"
	});
	//self.$outbox.appendTo(self.$outbox);
	var $back = $("<div></div>");
	$back.css({
		position: "absolute",
		right: "30px",
		top: "30px",
		height: "95px",
		width: "95px",
		backgroundPosition:" center",
		backgroundRepeat:" no-repeat",
		backgroundImage: "url(imgs/map/back.png)",
		cursor:"pointer"
	});
	$back.appendTo(self.$outbox);
	$back.click(function(){
		director.runSence(new ModeSelect());
	});
	var $bannerbox = $("<div></div>");
	$bannerbox.css({
		height: "500px",
		width: "800px",
		position: "absolute",
		left: "calc(50% - 400px)",
		top: "10%"
	});
	$bannerbox.appendTo(self.$outbox);
	$("#bannerbox").find("div").css({transition:"all linear 0.2s"});
	$turnleft = $("<div></div>");
	$turnleft.css({
		"width":"47px",
		"height":"135px",
		"background":"url(imgs/map/left1.png) no-repeat center",
		"float":"left",
		"margin-top":"200px",
		"z-index":"100",
		cursor:"pointer"
	});
	$turnleft.appendTo($bannerbox);
	//右翻页
	$turnright = $("<div></div>");
	$turnright.css({
		"width":"47px",
		"height":"135px",
		"background":"url(imgs/map/left.png) no-repeat center",
		"float":"right",
		"margin-top":"200px",
		"z-index":"100",
		cursor:"pointer"
	});
	$turnright.appendTo($bannerbox);
	//创建图片素材
	//显示窗口
	$picviewer = $("<div></div>");
	$picviewer.appendTo($bannerbox);
	$picviewer.css({
		"top": "120px",
		"left": "60px",
		"width": "680px",
		"height":"300px",
		"position":"absolute",
		"overflow":"hidden"
	});
	//图片资源
	for( var i=0;i<6;i++){
		$showpic = $("<div></div>");
		$showpic.attr("id","map" + i);
		$showpic.css({
			"width":"300px",
			"height":"200px",
			"background":"url(imgs/map/map" + i + ".png) no-repeat center",
			"background-size":"87%",
			"position":"absolute",
			"top":"50px",
			"left": "200px",
			"z-index":"10","transition":"all 1s"
		});
		$showpic.appendTo($picviewer);
	}
	//左右按钮滚动图片
	self.nowid = 0;
	self.nowleft = 5;
	self.nowright = 1;

	var dire;
	picturn();
	//pinHover();
	this.onShow = function(){
		dire =3;
		picshow();
		picturn();
		pinHover();
	};
//图片下面小点
	//放入小圆点
	for(i=0;i<6;i++){
		var $spotDiv = $("<div></div>");
		$spotDiv.css({
			"height":"16px",
			"width":"16px",
			"float":"left",
			"position":"relative",
			"top":"450px",
			"left":"280px"
		});
		//个体小点设置id
		$spotDiv.attr("id","spot"+i);
		$spotDiv.attr("class","spot");
		//把小点加到$div里面
		$spotDiv.appendTo($bannerbox);
		$spotDiv.click(function(){
			//改样式
			$(".spot").each(function(){
				$(this).css("background","url(imgs/map/picbot.png)");
			});
			$(this).css("background","url(imgs/map/picbot-hover.png)");
			//切换图片
			self.nowid = parseInt(this.id.slice(-1));
			dire = 3;
			picshow();
			picturn();
		});
	}
		$turnleft.on("click",function(){
			dire = 0;
			picshow();
			picturn();

		});
		$turnright.on("click",function(){
			dire = 1;

			picshow();
			picturn();

		});
		//显示规则
		function picshow(){
			if(dire == 0){
				self.nowid--;
				self.nowleft = self.nowid - 1;
				self.nowright = self.nowid + 1;

				if(self.nowid == 0){
					self.nowleft = 5;
				}
				if(self.nowid == 5){
					self.nowright = 0;
				}
				if(self.nowid == 6){
					self.nowid = 0;
					self.nowleft = 5;
					self.nowright = 1;
				}
				if(self.nowid == -1){
					self.nowid = 5;
					self.nowleft = 4;
					self.nowright = 0;
				}
				pinHover();
			}

			if(dire == 1){
				self.nowid++;
				self.nowleft = self.nowid - 1;
				self.nowright = self.nowid + 1;

				if(self.nowid == 0){
					self.nowleft = 5;
				}
				if(self.nowid == 5){
					self.nowright = 0;
				}
				if(self.nowid == 6){
					self.nowid = 0;
					self.nowleft = 5;
					self.nowright = 1;
				}
				if(self.nowid == -1){
					self.nowid = 5;
					self.nowleft = 4;
					self.nowright = 0;
				}
				pinHover();
			}

			//点击小圆点的变化
			if(dire == 3){
				self.nowleft = self.nowid - 1;
				self.nowright = self.nowid + 1;
				if(self.nowid == 0){
					self.nowleft = 5;
					self.nowright = 1;
				}
				if(self.nowid == 5){
					self.nowleft = 4;
					self.nowright = 0;
				}
			}
		}
		//切换图片
		function picturn(){
			for(i=0;i<6;i++){
				$("#map" + i).css({
					"position":"absolute",
					"width":"300px",
					"height":"200px",
					"top":"50px",
					"left": "200px",
					"z-index":"10"
				});
			}
			$("#map" + self.nowid).css({
				"top":"0px",
				"width":"500px",
				"height":"300px",
				"left":"80px",
				"z-index":"30"
			});
			$("#map" + self.nowleft).css({
				"top":"50px",
				"left":"0px",
				"width":"300px",
				"height":"200px",
				"z-index":"20"
			});
			$("#map" + self.nowright).css({
				"width":"300px",
				"height":"200px",
				"top":"50px",
				"left":"360px",
				"z-index":"20"
			});
		}
		//小点变化
		function pinHover(){
			$(".spot").each(function(){
				$(this).css("background","url(imgs/map/picbot.png)");
			});
			$("#spot" + self.nowid).css("background","url(imgs/map/picbot-hover.png)");
		}
		pinHover();
	}