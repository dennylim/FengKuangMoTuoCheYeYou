/**
 * Created by Administrator on 2016/3/11.
 */
function RegisterSence(){
    var self = this;
    //外面盒子相当于body
    self.$outbox = $("<div class='outbox'></div>");
    self.$outbox.css({
        "background-image": "url(imgs/login_bg.jpg)"
    });
    var $outdiv =$("<div></div>");
    $outdiv.css({
        width:"509px",
        height:"424px",
        display: "inline-block",
        position:"absolute",
        top:"18%",
        left:"30%"
    });
    $outdiv.appendTo(this.$outbox);
    var $return = $("<div id='return'></div>");
    $return.css({
        position: "absolute",
        top: "3%",
        right:"10%",
        cursor: "pointer",
        width:"95px",
        height:"95px",
        "background-image":"url(imgs/return.png)"
    });
    $return.appendTo(this.$outbox);
    $return.click(function(){
        director.runSence(new LoginSence());
    });
    var $dialog = $("<form></form>");
    $dialog.attr("id", "dialogreg");
    $dialog.css({
        "background-image": "url(imgs/login_bg1.png)",
        width: "509px",
        height: "424px",
        margin: "0 auto",
        overflow: "hidden"
    });
    $dialog.appendTo($outdiv);
    //第一个输入框
    var $line1=$("<div></div>");
    $line1.css({
        margin: "0 auto",
        width: "387px",
        height: "62px",
        position: "relative",
        "margin-top": "35px",
        "background-image": "url(imgs/input.png)"
    });
    $line1.appendTo($dialog);
    var $input1=$("<input/>",{
        id:'zhucename',
        type:'text',
        name:'zhucename',
        placeholder:'请输入账号'
    });
    $input1.css({
        "background-color": "black",
        color: "white",
        width: "210px",
        height: "35px",
        "font-size": "20px",
        position: "absolute",
        left: "0px",
        "margin-left": "50px",
        "margin-top": "10px"
    });
    $input1.appendTo($line1);
    //第二个输入框
    var $line2=$("<div></div>");
    $line2.css({
        margin: "0 auto",
        width: "387px",
        height: "62px",
        position: "relative",
        "margin-top": "35px",
        "background-image": "url(imgs/input.png)"
    });
    $line2.appendTo($dialog);
    var $input2=$("<input/>",{
        id:'pwd1',
        type:'password',
        name:'pwd1',
        placeholder:'请输入密码'
    });
    $input2.css({
        "background-color": "black",
        color: "white",
        width: "210px",
        height: "35px",
        "font-size": "20px",
        position: "absolute",
        left: "0px",
        "margin-left": "50px",
        "margin-top": "10px"
    });
    $input2.appendTo($line2);
    //第三个输入框
    var $line3=$("<div></div>");
    $line3.css({
        margin: "0 auto",
        width: "387px",
        height: "62px",
        position: "relative",
        "margin-top": "35px",
        "background-image": "url(imgs/input.png)"
    });
    $line3.appendTo($dialog);
    var $input3=$("<input/>",{
        id:'pwd2',
        type:'password',
        name:'pwd2',
        placeholder:'请再次输入密码'
    });
    $input3.css({
        "background-color": "black",
        color: "white",
        width: "210px",
        height: "35px",
        "font-size": "20px",
        position: "absolute",
        left: "0px",
        "margin-left": "50px",
        "margin-top": "10px"
    });
    $input3.appendTo($line3);
    //注册新账号按钮
    var $line4=$("<div id='reg'></div>");
    $line4.css({
        display: "inline-block",
        "margin-left": "123px",
        "margin-top":" 20px",
        cursor: "pointer",
        width:"263px",
        height:"69px",
        "background-image":"url(imgs/new_reg.png)"
    });
    $line4.appendTo($dialog);
    $dialog.validate({
        rules:{
            zhucename:{
                required:true,
                minlength:5,
                maxlength:10
            },
            pwd1:{
                required:true,
                rangelength:[6,10],
                digits:true
            },
            pwd2:{
                required:true,
                rangelength:[6,10],
                digits:true,
                equalTo: "input[name=pwd1]"
            }
        },
        messages:{
            zhucename: {
                requied:"用户名至少5个字符最多10个字符"
            },
            pwd1: {
                requied: "密码范围在6~10位之间"

            },
            pwd2:{
                requied: "两次输入密码需一致"
            }
        }
    });
    $line4.click(function() {
        $dialog.valid();
        var $zhucename = $("#zhucename").val();
        var $pwd1 = $("#pwd1").val();
        var $pwd2 = $("#pwd2").val();
        var len = 0, i;
        db.transaction(function (tx) {
            tx.executeSql('SELECT users.login log FROM users', [], function (tx, results) {
                len = results.rows.length;
                for (i = 0; i < len; i++) {
                    if ($zhucename == results.rows.item(i).log) {
                        alert("用户已存在，注册失败！");
                        break;
                    }else if( $pwd1 != $pwd2 ){
                        alert("两次输入的密码不一致，注册失败！");
                        break;
                    }else if( $zhucename == '' ){
                        alert("账号不能为空!");
                        break;
                    } else if( $pwd1=='' || $pwd2=='' ){
                        alert("密码不能为空!");
                        break;
                    }
                }
            }, null);
        });
        db.transaction(function (tx) {
            if(i == len && $pwd1 == $pwd2){
                tx.executeSql("INSERT INTO [users] ([login], [password], [u_name], [level], [money], [online_time], [last_time]) VALUES (?,?, ?, 1, 0, null, null)",[$zhucename, $pwd1, $zhucename]);
                alert("注册成功！！")
            }
        });
    });
}