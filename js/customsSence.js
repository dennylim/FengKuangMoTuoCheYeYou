/**
 * Created by Administrator on 2016/3/15.
 */
function CustomsChoose(){
    var self = this;
    self.$outbox = $("<div class='outbox'></div>");
    self.$outbox.css({"background-image":"url(imgs/customsChoose.jpg)"});
    var $return = $("<div></div>");
    $return.css({
        position: "absolute",
        top: "20%",
        right:"530px",
        cursor: "pointer",
        width:"95px",
        height:"95px",
        "background-image":"url(imgs/return.png)"
    });
    $return.appendTo(self.$outbox);
    var $customsbox = $("<div></div>");
    $customsbox.css({
        "background-image":"url(imgs/customsBox.png)",
        width:"710px",
        height:"500px",
        position:"absolute",top:"10%",left:"25%"
    });
    $customsbox.appendTo(self.$outbox);
    var $level1 = $("<div></div>");
    $level1.css({
        "background-image":"url(imgs/passedCustoms.png)",
        width:"83px",
        height:"83px",
        position:"absolute",
        bottom:"10%",
        left:"8%",
        "z-index":"10"
    });
    $level1.appendTo($customsbox);
    var $level2 = $level1.clone();
    $level2.css({bottom:"20%",left:"30%","background-image":"url(imgs/lockCustoms.png)"});
    $level2.appendTo($customsbox);
    var $level3 = $level1.clone();
    $level3.css({bottom:"45%",left:"13%","background-image":"url(imgs/lockCustoms.png)"});
    $level3.appendTo($customsbox);
    var $level4 = $level1.clone();
    $level4.css({top:"15%",left:"18%","background-image":"url(imgs/lockCustoms.png)"});
    $level4.appendTo($customsbox);
    var $level5 = $level1.clone();
    $level5.css({top:"30%",left:"35%","background-image":"url(imgs/lockCustoms.png)"});
    $level5.appendTo($customsbox);
    var $level6 = $level1.clone();
    $level6.css({top:"14%",left:"45%","background-image":"url(imgs/lockCustoms.png)"});
    $level6.appendTo($customsbox);
    var $level7 = $level1.clone();
    $level7.css({top:"10%",left:"65%","background-image":"url(imgs/lockCustoms.png)"});
    $level7.appendTo($customsbox);
    var $level8 = $level1.clone();
    $level8.css({top:"25%",left:"80%","background-image":"url(imgs/lockCustoms.png)"});
    $level8.appendTo($customsbox);
    var $level9 = $level1.clone();
    $level9.css({top:"47%",left:"75%","background-image":"url(imgs/lockCustoms.png)"});
    $level9.appendTo($customsbox);
    var $level10 = $level1.clone();
    $level10.css({bottom:"5%",left:"75%","background-image":"url(imgs/lockCustoms.png)"});
    $level10.appendTo($customsbox);
}