/**
 * Created by Administrator on 2016/3/15.
 */
function BalanceSence(){
    var self = this;
    //外面盒子相当于body
    self.$outbox = $("<div class='outbox'></div>");

    var $top = $("<div></div>");
    $top.css({
        width:"100%",
        height:"80%",
        "background-image":"url(imgs/moushixz_bg.jpg)",
        position:"absolute",
        "background-repeat":"no-repeat",
        "background-size":"cover",
        "backgrond-position":"bottom",
        "overflow":"hidden",
        "min-width":"900px",
        "min-height":"535px",
        top:"0"
    });
    $top.appendTo(self.$outbox);
    //墙
    var $wall = $("<div></div>");
    $wall.css({
        width:"913px",
        height:"444px",
        "background-image":"url(imgs/jiesuanwall.png)",
        "background-repeat":"no-repeat",
        position:"absolute",
        bottom:"20%",
        left:"calc(50% - 456px)"
    });
    $wall.appendTo(self.$outbox);

    //难度
    var $dif = $("<span></span>");
    $dif.css({
        width:"85px",
        height:"19px",
        color:"white",
        "font-size":"17px",
        "font-family":"微软雅黑",
        "text-align":"center",
        position:"absolute",
        top:"5%",
        left:"68%"
    });
    $dif.html("难度：3星");
    $dif.appendTo($wall);
    //金币
    var $coin = $dif.clone();
    $coin.css({left:"78%"});
    $coin.html("金币：120");
    $coin.appendTo($wall);
    //名次
    var $mingci = $dif.clone();
    $mingci.css({left:"87%"});
    $mingci.html("名次：6");
    $mingci.appendTo($wall);
    //金币图片
    var $coinimg = $("<div></div>");
    $coinimg.css({
        width:"47px",
        height:"51px",
        "background-image":"url(imgs/jinb2.png)",position:"absolute",top:"12%",left:"68%"
    });
    $coinimg.appendTo($wall);
    //金币总数
    var $all = $("<span></span>");
    $all.css({
        "font-size":"29px",
        "font-family":"微软雅黑",
        "text-align":"center",
        width:"200px",color:"white",
        height:"32px",
        position:"absolute",top:"13%",left:"73%"
    });
    $all.html("总数：123456");
    $all.appendTo($wall);
    //第几名
    var $place = $("<span></span>");
    $place.css({
        width:"150px",heigt:"40px",position:"absolute",top:"30%",left:"73%","font-size":"50px",
        "font-family":"微软雅黑","text-fill-color":"white","text-stroke-color":"black","text-stroke-width":"1px",
        "text-align":"center","font-weight":"bolder"
    });
    $place.html("第四名");
    $place.appendTo($wall);
    //比赛所用时间
    var $time4 = $("<span></span>");
    $time4.css({width:"105px",position:"absolute",height:"24px",left:"74.5%",top:"45%",fontSize:"30px","text-fill-color":"red",
        "text-stroke-color":"black","text-stroke-width":"0.5px",
        "font-weight":"bolder"});
    $time4.html("00:08:29");
    $time4.appendTo($wall);
    var $time1 = $time4.clone();
    $time1.css({
        top:"92%",left:"9%","text-fill-color":"#ffc600"
    });
    $time1.html("00:06:30");
    $time1.appendTo($wall);
    var $time2 = $time4.clone();
    $time2.css({top:"92%",left:"29%","text-fill-color":"#ffc600"});
    $time2.html("00:05:48");
    $time2.appendTo($wall);
    var $time3 = $time4.clone();
    $time3.css({top:"92%",left:"49%","text-fill-color":"#ffc600"});
    $time3.html("00:07:48");
    $time3.appendTo($wall);
    //左一车手
    var $rider1 = $("<div></div>");
    $rider1.css({
        width:"153px",
        height:"205px",
        position:"absolute",
        top:"34.5%",
        left:"7%",
        "background-image": "url(imgs/player4.png)"
    });
    $rider1.appendTo($wall);
    //左二车手
    var $rider2 = $rider1.clone();
    $rider2.css({
        width:"138px",
        height:"209px",
        top:"27%",
        left:"25%",
        "background-image": "url(imgs/player1.png)"
    });
    $rider2.appendTo($wall);
    //左三车手
    var $rider3 = $rider1.clone();
    $rider3.css({
        width:"111px",
        height:"197px",
        top:"43%",
        left:"48%",
        "background-image": "url(imgs/player3.png)"
    });
    $rider3.appendTo($wall);
    //右一车手
    var $rider4 = $rider1.clone();
    $rider4.css({
        width:"112px",
        height:"165px",
        top:"63%",
        left:"74%",
        "background-image": "url(imgs/player2.png)"
    });
    $rider4.appendTo($wall);
    //左一奖杯
    var $cup1 = $("<div></div>");
    $cup1.css({
        width:"63px",
        height:"82px",
        position:"absolute",
        top:"63%",
        left:"19%",
        "background-image": "url(imgs/cup2.png)"
    });
    $cup1.appendTo($wall);
    //中间奖杯
    var $cup2 = $cup1.clone();
    $cup2.css({
        top:"55.5%",
        left:"38%",
        "background-image": "url(imgs/cup1.png)"
    });
    $cup2.appendTo($wall);
    //右一奖杯
    var $cup3 = $cup1.clone();
    $cup3.css({
        top:"69%",
        left:"57%",
        "background-image": "url(imgs/cup3.png)"
    });
    $cup3.appendTo($wall);
    //底部
    var $bottom = $("<div></div>");
    $bottom.css({
        "background-color":"black",
        width:"100%",
        height:"20%",
        position:"absolute",
        bottom:0
    });
    $bottom.appendTo(self.$outbox);
    //底部盒子
    var $botbox=$("<div></div>");
    $botbox.css({
        width:"900px",
        height:"69px",
        position:"absolute",
        left:"calc(50% - 450px)",
        top:"calc(50% - 34.5px)"
    });
    $botbox.appendTo($bottom);
    //重来一局按钮
    var $restart = $("<div></div>");
    $restart.css({
        width:"263px",
        height:"69px",
        float:"left",
        //position:"absolute",
        ////bottom:"3%",
        //left:"18%",
        "background-image": "url(imgs/restart.png)",
        cursor:"pointer"
    });
    $restart.appendTo($botbox);
    //回到菜单按钮
    var $returnmenu = $restart.clone();
    $returnmenu.css({
        "background-image": "url(imgs/returnmenu.png)","margin-left":"55px"
    });
    $returnmenu.appendTo($botbox);
    //下一关按钮
    var $nextlevel = $restart.clone();
    $nextlevel.css({
        "background-image": "url(imgs/nextlevel.png)","margin-left":"55px"
    });
    $nextlevel.appendTo($botbox);
}
