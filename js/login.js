/**
 * Created by Administrator on 2016/3/15.
 */
function LoginSence(){
    var self = this;
    //外面盒子相当于body
    self.$outbox = $("<div class='outbox'></div>");
    self.$outbox.css({
        "background-image": "url(imgs/login_bg.jpg)"
    });
    //对话框外面的一层
    var $outdiv=$("<div></div>");
    $outdiv.attr("id","dialoglog");
    $outdiv.css({
        height: "424px",
        "vertical-align": "middle",
        width: "calc(100% - 20px)",
        display: "inline-block"
    });
    $outdiv.appendTo(this.$outbox);
    //对话框
    var $dialog=$("<form></form>");
    $dialog.attr("id","dialoglog");
    $dialog.css({
        "background-image": "url(imgs/login_bg1.png)",
        width: "509px",
        height: "424px",
        margin: "0  auto",
        overflow: "hidden"
    });
    $dialog.appendTo($outdiv);
    //账号输入框
    var $line1=$("<div></div>");
    $line1.css({
        margin: "0 auto",
        width: "387px",
        height: "62px",
        position: "relative",
        "margin-top": "35px",
        "background-image": "url(imgs/input.png)"
    });
    $line1.appendTo($dialog);
    var $img1=$("<div></div>");
    $img1.css({
        "position": "absolute",
        "margin-left": "45px",
        "top": "15px",
        width:"67px",
        height:"29px",
        "background-image":"url(imgs/account.png)"
    });
    $img1.appendTo($line1);
    var $input1=$("<input/>",{
        id:'account',
        type:'text',
        name:'account',
        placeholder:'请输入账号'
    });
    $input1.css({
        "background-color": "black",
        color: "white",
        width: "210px",
        height: "35px",
        "font-size": "20px",
        position: "absolute",
        right: "0px",
        "margin-right": "50px",
        "margin-top": "10px"
    });
    $input1.appendTo($line1);
    //密码输入框
    var $line2=$("<div></div>");
    $line2.css({
        margin: "0 auto",
        width: "387px",
        height: "62px",
        position: "relative",
        "margin-top": "35px",
        "background-image": "url(imgs/input.png)"
    });
    $line2.appendTo($dialog);
    var $img2=$("<div></div>");
    $img2.css({
        "position": "absolute",
        "margin-left": "45px",
        "top": "15px",
        width:"67px",
        height:"29px",
        "background-image":"url(imgs/pwd.png)"
    });
    $img2.appendTo($line2);
    var $input2=$("<input/>",{
        id:'pwd',
        type:'password',
        name:'pwd',
        placeholder:'请输入密码'
    });
    $input2.css({
        "background-color": "black",
        color: "white",
        width: "210px",
        height: "35px",
        "font-size": "20px",
        position: "absolute",
        right: "0px",
        "margin-right": "50px",
        "margin-top": "10px"
    });
    $input2.appendTo($line2);
    //登录按钮
    var $line3=$("<div id='log' type='submit'></div>");
    $line3.css({
        display: "inline-block",
        "margin-left": "123px",
        "margin-top":" 30px",
        cursor: "pointer",
        width:"263px",
        height:"69px",
        "background-image":"url(imgs/log.png)"
    });
    $line3.appendTo($dialog);
    //注册账号按钮
    var $line4=$("<div id='reg'></div>");
    $line4.css({
        display: "inline-block",
        "margin-left": "123px",
        "margin-top":" 20px",
        cursor: "pointer",
        width:"263px",
        height:"69px",
        "background-image":"url(imgs/reg.png)"
    });
    $line4.click(function(){
        director.runSence(new RegisterSence());
    });
    $line4.appendTo($dialog);
    var $help=$("<div id='help'></div>");
    $help.appendTo($outdiv);
    $help.css({
        position: "absolute",
        top: "72%",
        left:"10px",
        cursor: "pointer",
        width:"95px",
        height:"95px",
        "background-image":"url(imgs/help.png)"
    });
    var $voice=$("<div id='voice'></div>");
    $voice.css({
        position: "absolute",
        top: "72%",
        right:"10px",
        cursor: "pointer",
        width:"95px",
        height:"95px",
        "background-image":"url(imgs/voice.png)"
    });
    $voice.appendTo($outdiv);

    $line3.click(function(){
        $dialog.valid();
        var $account=$("#account").val();
        var $pwd=$("#pwd").val();
        db.transaction(function (tx) {
            tx.executeSql('SELECT users.login log,users.password pwd,users.id id from users', [], function (tx, results) {
                var len = results.rows.length,i;
                for (i = 0; i < len; i++){
                    if(results.rows.item(i).log == $account){
                        if($pwd == results.rows.item(i).pwd){
                            localStorage.nowlogin=results.rows.item(i).id;
                            alert("登录成功！！");
                            director.runSence(new Loading());


                            break;
                        }
                    }
                    else{
                        alert("用户名或密码错误，请重新输入！！");
                        break;
                    }
                }
            },null);
        });
    });
    $dialog.validate({
        rules:{
            account:{
                required:true,
                minlength:5,
                maxlength:10
            },
            pwd:{
                required:true,
                rangelength:[6,10]
            }
        },
        messages:{
            account: {
                requied:"用户名至少5个字符最多10个字符"
            },
            pwd: {
                requied: "密码范围在6~10位之间"
            }
        }
    });

}