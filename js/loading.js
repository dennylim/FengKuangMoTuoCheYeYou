/**
 * Created by Administrator on 2016/3/15.
 */
function Loading(){
    var self = this ;
    //外面盒子相当于body
    this.$outbox = $("<div class='outbox'></div>");
    this.$outbox.css({
        "background-image": "url(imgs/loading.jpg)"
    });

    var $probarbox = $("<div></div>");
    $probarbox.css({
        width:"100%",height:90,position:"absolute",top:"60%","z-index":"10"
    });
    $probarbox.appendTo(this.$outbox);
    var $motoimg = $("<div class='motomove'></div>");
    $motoimg.css({
        "background-image":"url(imgs/moto.png)",
        width:"91px",
        height:"47px",
        position:"absolute",
        top:"0%",
        left:"25%",
        "z-index":"10"
    });
    $motoimg.appendTo($probarbox);
    var $outproBar = $("<div></div>");
    $outproBar.css({
        "background-image":"url(imgs/outproBar.png)",
        width:"744px",
        height:"36px",
        position:"absolute",
        top:"45%",
        left:"25%",
        "z-index":"10"
    });
    $outproBar.appendTo($probarbox);
    var $probar= $("<div class='probar'></div>");
    $probar.css({
        "background-image":"url(imgs/progressBar.png)",
        "background-size":"722px 19px",
        "background-repeat":"no-repeat",
        width:"0%",
        height:"19px",
        position:"absolute",
        top:"8px",
        left:"12px",
        "z-index":"10"
    });

    $probar.appendTo($outproBar);
    var $loadtext= $("<div></div>");
    $loadtext.css({
        "background-image":"url(imgs/loadText.png)",
        width:"176px",
        height:"27px",
        position:"absolute",
        bottom:"17%",
        left:"45%",
        "z-index":"10"
    });
    $loadtext.appendTo(this.$outbox);
    setTimeout(function() {
        director.runSence(new ModeSelect());
    },3500);
}