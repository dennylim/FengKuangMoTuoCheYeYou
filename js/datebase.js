/**
 * Created by Administrator on 2016/3/10.
 */
var db = openDatabase('HF151103', '1.0', 'my db', 2 * 1024);
db.transaction(function (tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS equipment (' +
        'id         INTEGER PRIMARY KEY AUTOINCREMENT,' +
        'name       TEXT    UNIQUE,' +
        'type       INTEGER NOT NULL,' +                    //类型，区别不同类型的装备
        'value_type INTEGER,' +                             //属性类型,车身影响maxspeed；轮子影响……
        'value      INTEGER,' +                             //数值
        'degree     INTEGER DEFAULT ( 1 ),' +               //不设置
        'price      INTEGER NOT NULL,' +                    //价格
        'img   TEXT  NOT NULL )'                            //图片
    );
});
db.transaction(function(tx){
    tx.executeSql('CREATE TABLE if not exists users ('+
        'id          INTEGER PRIMARY KEY AUTOINCREMENT,'+
        'login       TEXT    UNIQUE  NOT NULL,'+            //账号
        'password    TEXT    NOT NULL,'+                    //密码
        'u_name      TEXT    UNIQUE  NOT NULL,'+            //昵称
        'level       INTEGER default(0),'+                  //等级，不设置
        'money       INTEGER NOT NULL,'+                    //金币
        'online_time FLOAT,'+                               //在线时间，不设置
        'last_time   DATE)'                                 //上次登录的时间
    );
    tx.executeSql("INSERT INTO [users] ([id], [login], [password], [u_name], [level], [money], [online_time], [last_time]) VALUES (1, 'admin', 123456, 'qfwf', 1, 10, null, datetime('now'))");
    tx.executeSql("INSERT INTO [users] ([id], [login], [password], [u_name], [level], [money], [online_time], [last_time]) VALUES (3, 'HF151101', 123456, 'zz', 1, 0, null, null)");
    tx.executeSql("INSERT INTO [users] ([id], [login], [password], [u_name], [level], [money], [online_time], [last_time]) VALUES (4, 'HF151102', 123456, 'yy', 1, 0, null, null)");
});
db.transaction(function(tx){
    tx.executeSql('CREATE TABLE if not exists bikes ('+
        'id       INTEGER PRIMARY KEY AUTOINCREMENT,'+
        'name     TEXT,'+
        'addspeed INTEGER,'+
        'maxspeed INTEGER,'+
        'degree   INTEGER,'+
        'price    INTEGER NOT NULL,'+
        'img      TEXT)'
    );
});
db.transaction(function(tx){
    tx.executeSql('CREATE TABLE if not exists riders ('+    //骑手表--参赛的人物
        'id      INTEGER PRIMARY KEY AUTOINCREMENT,'+       //自动增长
        'r_name  TEXT    UNIQUE  NOT NULL,'+                //骑手名
        'img     TEXT    NOT NULL,'+                        //骑手图片分为：骑在车上的，站着的
        'skill   TEXT    NOT NULL,'+                        //技能（不设置）
        'price   INTEGER NOT NULL,'+                        //价格
        'r_level INTEGER)'                                  //骑手等级（不做）
    );
});
db.transaction(function(tx){
    tx.executeSql('CREATE TABLE if not exists mission ('+       //关卡表，赛事包括赛段
        'id       INTEGER PRIMARY KEY AUTOINCREMENT,'+          //关卡id，自动增长
        'name     TEXT,'+                                       //赛事、赛段名
        'descript TEXT,'+                                       //赛事描述--不设置
        'level    INTEGER,'+                                    //赛事等级--不设置
        'belong   INTEGER,'+                                    //从属关系，自关联上一级是谁，若为空，即为赛事；若有值，则为赛段
        'map      TEXT,'+                                       //游戏地图，img名字
        'conf     TEXT,'+                                       //文本，比赛的配置
        'img      TEXT )'                                       //banner赛事选择的图片
    );
    tx.executeSql("INSERT INTO mission (id,name,descript,level,belong,map,conf,img) VALUES (1,'陆地','yellow',1,1,null,null,null)");
    tx.executeSql("INSERT INTO mission (id,name,descript,level,belong,map,conf,img) VALUES (2,'天空','bule',2,1,null,null,null)");
    tx.executeSql("INSERT INTO mission (id,name,descript,level,belong,map,conf,img) VALUES (3,'大海','beauty',2,1,null,null,null)");
});
db.transaction(function(tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS user_equip (' +   //用户手上的装备表（装备实体）
        'userid INTEGER REFERENCES users ( id ),' +             //玩家id
        'equipid INTEGER REFERENCES equipment ( id ),' +        //装备id
        'blong_time DATE,' +                                    //拥有的时间 datetime('now')
        'now_value INTEGER,' +                                  //不设置
        'degree INTEGER,' +                                     //不设置
        'CONSTRAINT pk_ue PRIMARY KEY ( userid, equipid ))'     //用户id与装备id为合成主键
    );
    tx.executeSql("INSERT INTO [user_equip] ([userid], [equipid], [blong_time], [now_value], [degree]) VALUES (3, 1, '2016-03-09', null, null)");
    tx.executeSql("INSERT INTO [user_equip] ([userid], [equipid], [blong_time], [now_value], [degree]) VALUES (3, 2, '2016-03-09', null, null)");
    tx.executeSql("INSERT INTO [user_equip] ([userid], [equipid], [blong_time], [now_value], [degree]) VALUES (4, 2, '2016-03-09', null, null)");
    tx.executeSql("INSERT INTO [user_equip] ([userid], [equipid], [blong_time], [now_value], [degree]) VALUES (4, 1, '2016-03-09', null, null)");
});
db.transaction(function(tx){
    tx.executeSql('CREATE TABLE user_rider ('+              //用户拥有的骑手表
        'userid     INTEGER REFERENCES users ( id ),'+      //玩家id
        'riderid    INTEGER REFERENCES rider ( id ),'+      //骑手id
        'blong_time DATE,'+                                 //何时买的，datetime('now')
        'now_value  INTEGER,'+                              //不设置
        'degree     INTEGER,'+                              //不设置
        'CONSTRAINT pk_ur PRIMARY KEY ( userid, riderid ))' //用户id与骑手id合成主键
    );
});
db.transaction(function(tx){
    tx.executeSql('CREATE TABLE user_bike ('+               //用户拥有的车表      有较大的更改
        'id       INTEGER PRIMARY KEY AUTOINCREMENT,'+      //
        'userid     INTEGER REFERENCES users ( id ),'+      //玩家id
        'bikeid     INTEGER REFERENCES bike ( id ),'+       //车辆id
        'blong_time DATE,'+                                 //何时买的，datetime('now')
        'now_value  INTEGER,'+                              //不设置
        'degree     INTEGER,'+                              //不设置
        'CONSTRAINT pk_ub PRIMARY KEY ( userid, bikeid ))'
    );
});
db.transaction(function(tx){
    tx.executeSql('CREATE TABLE match_result ('+            //比赛结果表  记录所有玩过的关卡成绩
        'userid     INTEGER REFERENCES users ( id ),'+      ////玩家id
        'missionid  INTEGER REFERENCES mission ( id ),'+    //赛段id
        'match_time DATE,'+                                 //比赛结束时间
        'result     INTEGER,'+      //过关与否，0表示玩过没过关；1表示已过关1星，2表示2星
        'award      INTEGER,'+                              //奖励，金币数
        'CONSTRAINT pk_mr PRIMARY KEY ( userid, missionid, match_time ))'//三列合在一起作为复合主键，这三个值在表中不能一起重复；
    );
    tx.executeSql("INSERT INTO match_result (userid,missionid,match_time,result,award) VALUES (1,1,datetime('now'),1,null)");
    tx.executeSql("INSERT INTO match_result (userid,missionid,match_time,result,award) VALUES (1,2,datetime('now'),0,null)");
    tx.executeSql("INSERT INTO match_result (userid,missionid,match_time,result,award) VALUES (2,1,datetime('now'),0,null)");
    tx.executeSql("INSERT INTO match_result (userid,missionid,match_time,result,award) VALUES (3,1,datetime('now'),1,null)");
});
//db.transaction(function(tx){
//    tx.executeSql('CREATE TABLE user_mission(' +
//        'userid     INTEGER REFERENCES users ( id ),'+
//        'missionid  INTEGER REFERENCES mission ( id ),')+
//        'clear_time DATE)'
//    );
//});
