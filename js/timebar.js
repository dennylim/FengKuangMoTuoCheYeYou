/**
 * Created by Administrator on 2016/3/2.
 */
function TimeBar($parent){
    var self=this;
    this.min=0;
    this.sec=0;
    this.ms=0;
    self.running=false;
    self.stop=true;

    var $time=$("<div class='time'></div>");
    $time.css({
        width: "410px",
        height: "50px",
        "margin-top": "50px",
        "margin-left": "400px",
        position: "absolute",
        "background-color": "white",
        "z-index": "1000"
    })
    $time.appendTo($parent);
    self.$div=$("<div></div>");
    self.$div.html("00:00:00");
    self.$div.css({"fontSize":"20px","marginLeft":"10px"});
    self.$div.appendTo($time);
    self.$butGo=$("<button></button>");
    self.$butGo.html("Go");
    self.$butGo.css({"marginLeft":"10px"});
    self.$butGo.appendTo($time);
    self.$butStop=$("<button></button>");
    self.$butStop.html("Stop");
    self.$butStop.css({"marginLeft":"10px"});
    self.$butStop.appendTo($time);
    self.$butReset=$("<button></button>");
    self.$butReset.html("Reset");
    self.$butReset.css({"marginLeft":"10px"});
    self.$butReset.appendTo($time);
    //var mydate = new Date();
    //var mytime=mydate.toLocaleTimeString();
    //console.log(mytime);
    //var nowtime=document.getElementById("time");
    //nowtime.innerHTML("mytime");

    self.time=function(){
        if(self.ms==100){
            self.ms=0;
            self.sec++;
        }
        if(self.sec==60){
            self.sec=0;
            self. min++;
        }
        if(self.ms<10){
            self.ms="0"+self.ms;
        }
        if(self.sec<10){
            self.sec="0"+self.sec;
        }
        if(self.min<10){
            self.min="0"+self.min;
        }

        self.$div.html(self.min+":"+self.sec+":"+self.ms);
        self.min=parseInt(self.min);
        self.sec=parseInt(self.sec);
        self.ms=parseInt(self.ms);
        self.ms++;
    }
    self.$butGo.click(function(){
        if(self.running==false) {
            self.timer=setInterval(self.time,10);
            self.running=true;
            self.stop=false;
        }else{
            return;
        }

    });
    self.$butStop.click(function(){
        clearInterval(self.timer);
        self.running=false;
        self.stop=true;
    });
    self.$butReset.click(function(){
        clearInterval(self.timer);
        self.running=false;
        self.stop=true;
        self.$div.html("00:00:00");
        self.min=0;
        self.sec=0;
        self.ms=0;
    });
}
