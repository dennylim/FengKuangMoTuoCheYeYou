/**
 * Created by Administrator on 2016/3/15.
 */
function ShopSence(){
    var self = this;
    //外面盒子相当于body
    self.$outbox = $("<div class='outbox'></div>");
    self.$outbox.css({
        "background-image":"url(imgs/patternchoose.jpg)"
    });
    //预览框
    var $preview = $("<div></div>");
    $preview.css({
        width:"256px",
        height:"57px",
        position:"absolute",
        top:"5%",
        left:"19.5%",
        "z-index":"100",
        "background-image":"url(imgs/shop/previewbg.png)"
    });
    $preview.appendTo(self.$outbox);
    //预览文字的图片
    var $previewtext = $("<div></div>");
    $previewtext.css({
        width:"76px",
        height:"32px",
        position:"absolute",
        top:"20%",
        left:"35%",
        "z-index":"100",
        "background-image":"url(imgs/shop/preview.png)"
    });
    $previewtext.appendTo($preview);
    //玩家信息
    var $perInf = $("<div></div>");
    $perInf.css({
        width:"291px",
        height:"562px",
        position:"absolute",
        top:"9%",
        left:"18%",
        "z-index":"10",
        "background-image":"url(imgs/shop/shopleftbg.png)"
    });
    $perInf.appendTo(self.$outbox);
    //玩家账号
    var $playname= $("<div></div>");
    $playname.css({
        width:"117px",
        height:"29px",
        position:"absolute",
        top:"8%",
        left:"28%",
        "z-index":"10",
        "background-image":"url(imgs/shop/playername.png)"
    });
    $playname.appendTo($perInf);
    //个人背景图片
    var $perbg= $("<div></div>");
    $perbg.css({
        width:"257px",
        height:"267px",
        position:"absolute",
        top:"15%",
        left:"5%",
        "z-index":"10",
        "background-image":"url(imgs/shop/perbg.png)"
    });
    $perbg.appendTo($perInf);
    //骑手图片
    var $perimg= $("<div></div>");
    $perimg.css({
        width:"178px",
        height:"225px",
        position:"absolute",
        top:"8%",
        left:"15%",
        "z-index":"10",
        "background-image":"url(imgs/shop/playerimg.png)"
    });
    $perimg.appendTo($perbg);
    //玩家金币
    var $pergold= $("<div></div>");
    $pergold.css({
        width:"109px",
        height:"27px",
        position:"absolute",
        top:"65%",
        left:"13%",
        "z-index":"10",
        "background-image":"url(imgs/shop/mycoin.png)"
    });
    $pergold.appendTo($perInf);
    var $pergoldval = $("<div></div>");
    $pergoldval.css({
        width:"123px",height:"28px",position:"absolute",top:"64%",left:"53%","z-index":"10",
        "font-size":"28px","font-family":"微软雅黑",boder:"1px solid black","text-fill-color":"#ffc600","text-strock-color":"black",
        "text-stroke-width":"2px","font-weight":"bolder"
    });
    $pergoldval.html("12345");
    $pergoldval.appendTo($perInf);
    //我的车库按钮
    var $mygarage = $("<div></div>");
    $mygarage.css({
        width:"231px",
        height:"61px",
        position:"absolute",
        top:"73%",
        left:"10%",
        "z-index":"10",
        "background-image":"url(imgs/shop/mygarage.png)",
        cursor:"pointer"
    });
    $mygarage.appendTo($perInf);
    $mygarage.click(function(){
       director.runSence(new GarageSence());
    });
    //充值按钮
    var $chzh = $mygarage.clone();
    $chzh.css({
        top:"85%",
        "background-image":"url(imgs/shop/gamechzh.png)"
    });
    $chzh.appendTo($perInf);
    //商店框
    var $shopbg = $("<div></div>");
    $shopbg.css({
        width:"394px",
        height:"53px",
        position:"absolute",
        top:"5%",
        left:"48%",
        "z-index":"100",
        "background-image":"url(imgs/shop/shoptopinput.png)"
    });
    $shopbg.appendTo(self.$outbox);
    //商店文字的图像
    var $shoptext = $("<div></div>");
    $shoptext.css({
        width:"84px",
        height:"32px",
        position:"absolute",
        top:"20%",
        left:"38%",
        "z-index":"100",
        "background-image":"url(imgs/shop/shoptext.png)"
    });
    $shoptext.appendTo($shopbg);
    //返回按钮
    var $esc = $("<div></div>");
    $esc.css({
        width:"64px",
        height:"64px",
        position:"absolute",
        top:"4%",
        right:"15%",
        "z-index":"100",cursor:"pointer",
        "background-image":"url(imgs/shop/esc.png)"
    });
    $esc.appendTo(self.$outbox);
    $esc.click(function(){
        director.runSence(new ModeSelect());
    });
    //商店信息
    var $shopInf = $("<div></div>");
    $shopInf.css({
        width:"613px",
        height:"562px",
        position:"absolute",
        top:"9%",
        left:"40%",
        "z-index":"10",
        "background-image":"url(imgs/shop/shoprightbg.png)"
    });
    $shopInf.appendTo(self.$outbox);
    //
    var $person = $("<div></div>");
    $person.css({
        width:"143px",
        height:"104px",
        position:"absolute",
        top:"10%",
        left:"2%",
        "z-index":"10",cursor:"pointer",
        "background-image":"url(imgs/shop/hover.png)"
    });
    $person.appendTo($shopInf);
    //$person.click(function(){
    //    $person.css({"background-image":"url(imgs/shop/)"});
    //});
    //角色
    var $role= $("<div></div>");
    $role.css({
        width:"72px",
        height:"86px",
        position:"absolute",
        top:"9%",
        left:"23%",
        "z-index":"10",cursor:"pointer",
        "background-image":"url(imgs/shop/roleimg.png)"
    });
    $role.appendTo($person);
    $role.click(function () {
        $person.css({"background-image":"url(imgs/shop/hover.png)"});
        $bike.css({"background-image":"none"});
        $moto.css({"background-image":"url(imgs/shop/motolistcom.png)"});
        $wheelbox.css({"background-image":"none"});
        $wheel.css({"background-image":"url(imgs/shop/wheellistcom.png)"});
        //商店右背景图
        var $perRbg=$("<div></div>");
        $perRbg.css({
            width:"473px",
            height:"510px",
            position:"absolute",
            top:"6%",
            left:"22%",
            "z-index":"10",
            "background-image":"url(imgs/shop/rightlistbg.png)"
        });
        $perRbg.appendTo($shopInf);
        //商店列表
        var $perListbg=$("<div></div>");
        $perListbg.css({
            width:"459px",
            height:"428px",
            position:"absolute",
            top:"2%",
            left:"1%",
            "z-index":"10",
            "background-image":"url(imgs/shop/shopbg.jpg)"
        });
        $perListbg.appendTo($perRbg);
        //商店具体的商品框
        var $persondiv=$("<div></div>");
        $persondiv.css({
            width:"140px",
            height:"204px",
            position:"absolute",
            top:"1%",
            left:"2%",
            "z-index":"10",
            "background-image":"url(imgs/shop/shopproductbg.png)"
        });
        $persondiv.appendTo($perListbg);
        //产品名
        var $perName=$("<div></div>");
        $perName.css({
            width:"82px",
            height:"20px",
            position:"absolute",
            top:"4%",
            left:"20%",
            "z-index":"10",
            "background-image":"url(imgs/shop/shopname.png)"
        });
        $perName.appendTo($persondiv);
        //产品背景
        var $perbg=$("<div></div>");
        $perbg.css({
            width:"106px",
            height:"106px",
            position:"absolute",
            top:"15%",
            left:"10%",
            "z-index":"10",cursor:"pointer",
            "background-image":"url(imgs/shop/shopPersonbg.png)"
        });
        $perbg.appendTo($persondiv);
        //商店商品的图片
        var $personimg=$("<div></div>");
        $personimg.css({
            width:"62px",
            height:"89px",
            position:"absolute",
            top:"7%",
            left:"16%",
            "z-index":"10",cursor:"pointer",
            "background-image":"url(imgs/shop/shopPersen1.png)"
        });
        $personimg.appendTo($perbg);
        //商品价格
        var $perprice=$("<div></div>");
        $perprice.css({
            width:"114px",
            height:"16px",
            position:"absolute",
            top:"70%",
            left:"7%",
            "z-index":"10",
            "background-image":"url(imgs/shop/price.png)"
        });
        $perprice.appendTo($persondiv);
        //购买按钮
        var $buttbuy=$("<div></div>");
        $buttbuy.css({
            width:"62px",
            height:"28px",
            position:"absolute",
            top:"81%",
            left:"3%",
            "z-index":"10",
            "background-image":"url(imgs/shop/buy.png)",
            cursor:"pointer"
        });
        $buttbuy.appendTo($persondiv);
        //预览按钮
        var $buttpreview=$buttbuy.clone();
        $buttpreview.css({
            left:"51%",
            "background-image":"url(imgs/shop/look.png)"
        });
        $buttpreview.appendTo($persondiv);
    });
    //摩托盒子
    var $bike = $("<div></div>");
    $bike.css({
        width:"143px",
        height:"104px",
        position:"absolute",
        top:"37%",
        left:"2%",
        "z-index":"10",cursor:"pointer"
    });
    $bike.appendTo($shopInf);

    //摩托图片
    var $moto = $role.clone();
    $moto.css({
        width:"91px",
        height:"81px",
        left:"9%",top:"14%",
        "background-image":"url(imgs/garagemoto.png)"
    });
    $moto.appendTo($bike);
    $moto.click(function(){
        $person.css({"background-image":"none"});
        $bike.css({"background-image":"url(imgs/shop/hover.png)"});
        $moto.css({"background-image":"url(imgs/shop/motolisthover.png)"});
        $wheelbox.css({"background-image":"none"});
        $wheel.css({"background-image":"url(imgs/shop/wheellistcom.png)"});
        //商店右背景图
        var $motoRbg=$("<div></div>");
        $motoRbg.css({
            width:"473px",
            height:"510px",
            position:"absolute",
            top:"6%",
            left:"22%",
            "z-index":"10",
            "background-image":"url(imgs/shop/rightlistbg.png)"
        });
        $motoRbg.appendTo($shopInf);
        //商店列表
        var $motoListbg=$("<div></div>");
        $motoListbg.css({
            width:"459px",
            height:"428px",
            position:"absolute",
            top:"2%",
            left:"1%",
            "z-index":"10",
            "background-image":"url(imgs/shop/shopbg.jpg)"
        });
        $motoListbg.appendTo($motoRbg);
        //商店具体的商品框
        var $motodiv=$("<div></div>");
        $motodiv.css({
            width:"140px",
            height:"204px",
            position:"absolute",
            top:"1%",
            left:"2%",
            "z-index":"10",
            "background-image":"url(imgs/shop/shopproductbg.png)"
        });
        $motodiv.appendTo($motoListbg);
        //产品名
        var $motoName=$("<div></div>");
        $motoName.css({
            width:"82px",
            height:"20px",
            position:"absolute",
            top:"4%",
            left:"20%",
            "z-index":"10",
            "background-image":"url(imgs/shop/shopname.png)"
        });
        $motoName.appendTo($motodiv);
        //产品背景
        var $motobg=$("<div></div>");
        $motobg.css({
            width:"106px",
            height:"106px",
            position:"absolute",
            top:"15%",
            left:"10%",
            "z-index":"10",cursor:"pointer",
            "background-image":"url(imgs/shop/shopPersonbg.png)"
        });
        $motobg.appendTo($motodiv);
        //商店商品的图片
        var $motoimg=$("<div></div>");
        $motoimg.css({
            width:"100px",
            height:"89px",
            position:"absolute",
            top:"10%",
            left:"2%",
            "z-index":"10",cursor:"pointer",
            "background-image":"url(imgs/shop/motolisthover.png)","background-size":"100 89","background-position":"center",
            "background-repeat":"no-repeat"
        });
        $motoimg.appendTo($motobg);
        //商品价格
        var $motoprice=$("<div></div>");
        $motoprice.css({
            width:"114px",
            height:"16px",
            position:"absolute",
            top:"70%",
            left:"7%",
            "z-index":"10",
            "background-image":"url(imgs/shop/price.png)"
        });
        $motoprice.appendTo($motodiv);
        //购买按钮
        var $buttbuy=$("<div></div>");
        $buttbuy.css({
            width:"62px",
            height:"28px",
            position:"absolute",
            top:"81%",
            left:"3%",
            "z-index":"10",
            "background-image":"url(imgs/shop/buy.png)",
            cursor:"pointer"
        });
        $buttbuy.appendTo($motodiv);
        //预览按钮
        var $buttpreview=$buttbuy.clone();
        $buttpreview.css({
            left:"51%",
            "background-image":"url(imgs/shop/look.png)"
        });
        $buttpreview.appendTo($motodiv);


    });
    //轮子盒子
    var $wheelbox = $("<div></div>");
    $wheelbox.css({
        width:"143px",height:"104px",position:"absolute",top:"60%",left:"2%"
    });
    $wheelbox.appendTo($shopInf);
    //轮子图片
    var $wheel = $role.clone();
    $wheel.css({"background-image":"url(imgs/shop/wheellistcom.png)",width:"59px",height:87});
    $wheel.appendTo($wheelbox);
    $wheel.click(function(){
        $person.css({"background-image":"none"});
        $bike.css({"background-image":"none"});
        $moto.css({"background-image":"url(imgs/shop/motolistcom.png)"});
        $wheelbox.css({"background-image":"url(imgs/shop/hover.png)"});
        $wheel.css({"background-image":"url(imgs/shop/wheellisthover.png)"});
        //商店右轮子背景图
        var $wheelRbg=$("<div></div>");
        $wheelRbg.css({
            width:"473px",
            height:"510px",
            position:"absolute",
            top:"6%",
            left:"22%",
            "z-index":"10",
            "background-image":"url(imgs/shop/rightlistbg.png)"
        });
        $wheelRbg.appendTo($shopInf);
        //商店列表
        var $wheelListbg=$("<div></div>");
        $wheelListbg.css({
            width:"459px",
            height:"428px",
            position:"absolute",
            top:"2%",
            left:"1%",
            "z-index":"10",
            "background-image":"url(imgs/shop/shopbg.jpg)"
        });
        $wheelListbg.appendTo($wheelRbg);
        //商店具体的商品框
        var $wheeldiv=$("<div></div>");
        $wheeldiv.css({
            width:"140px",
            height:"204px",
            position:"absolute",
            top:"1%",
            left:"2%",
            "z-index":"10",
            "background-image":"url(imgs/shop/shopproductbg.png)"
        });
        $wheeldiv.appendTo($wheelListbg);
        //产品名
        var $wheelName=$("<div></div>");
        $wheelName.css({
            width:"82px",
            height:"20px",
            position:"absolute",
            top:"4%",
            left:"20%",
            "z-index":"10",
            "background-image":"url(imgs/shop/shopname.png)"
        });
        $wheelName.appendTo($wheeldiv);
        //产品背景
        var $wheelbg=$("<div></div>");
        $wheelbg.css({
            width:"106px",
            height:"106px",
            position:"absolute",
            top:"15%",
            left:"10%",
            "z-index":"10",cursor:"pointer",
            "background-image":"url(imgs/shop/shopPersonbg.png)"
        });
        $wheelbg.appendTo($wheeldiv);
        //商店商品的图片
        var $wheelimg=$("<div></div>");
        $wheelimg.css({
            width:"62px",
            height:"89px",
            position:"absolute",
            top:"7%",
            left:"16%",
            "z-index":"10",cursor:"pointer",
            "background-image":"url(imgs/shop/wheellisthover.png)","background-size":"62 89","background-position":"center",
            "background-repeat":"no-repeat"
        });
        $wheelimg.appendTo($wheelbg);
        //商品价格
        var $wheelprice=$("<div></div>");
        $wheelprice.css({
            width:"114px",
            height:"16px",
            position:"absolute",
            top:"70%",
            left:"7%",
            "z-index":"10",
            "background-image":"url(imgs/shop/price.png)"
        });
        $wheelprice.appendTo($wheeldiv);
        //购买按钮
        var $buttbuy=$("<div></div>");
        $buttbuy.css({
            width:"62px",
            height:"28px",
            position:"absolute",
            top:"81%",
            left:"3%",
            "z-index":"10",
            "background-image":"url(imgs/shop/buy.png)",
            cursor:"pointer"
        });
        $buttbuy.appendTo($wheeldiv);
        //预览按钮
        var $buttpreview=$buttbuy.clone();
        $buttpreview.css({
            left:"51%",
            "background-image":"url(imgs/shop/look.png)"
        });
        $buttpreview.appendTo($wheeldiv);

    });
    //商店右背景图
    var $shopRbg=$("<div></div>");
    $shopRbg.css({
        width:"473px",
        height:"510px",
        position:"absolute",
        top:"6%",
        left:"22%",
        "z-index":"10",
        "background-image":"url(imgs/shop/rightlistbg.png)"
    });
    $shopRbg.appendTo($shopInf);
    //商店列表
    var $shopListbg=$("<div></div>");
    $shopListbg.css({
        width:"459px",
        height:"428px",
        position:"absolute",
        top:"2%",
        left:"1%",
        "z-index":"10",
        "background-image":"url(imgs/shop/shopbg.jpg)"
    });
    $shopListbg.appendTo($shopRbg);
    //商店具体的商品框
    var $shopdiv=$("<div></div>");
    $shopdiv.css({
        width:"140px",
        height:"204px",
        position:"absolute",
        top:"1%",
        left:"2%",
        "z-index":"10",
        "background-image":"url(imgs/shop/shopproductbg.png)"
    });
    $shopdiv.appendTo($shopListbg);
    //产品名
    var $productName=$("<div></div>");
    $productName.css({
        width:"82px",
        height:"20px",
        position:"absolute",
        top:"4%",
        left:"20%",
        "z-index":"10",
        "background-image":"url(imgs/shop/shopname.png)"
    });
    $productName.appendTo($shopdiv);
    //产品背景
    var $productbg=$("<div></div>");
    $productbg.css({
        width:"106px",
        height:"106px",
        position:"absolute",
        top:"15%",
        left:"10%",
        "z-index":"10",cursor:"pointer",
        "background-image":"url(imgs/shop/shopPersonbg.png)"
    });
    $productbg.appendTo($shopdiv);
    //商店商品的图片
    var $productimg=$("<div></div>");
    $productimg.css({
        width:"62px",
        height:"89px",
        position:"absolute",
        top:"7%",
        left:"16%",
        "z-index":"10",cursor:"pointer",
        "background-image":"url(imgs/shop/shopPersen1.png)"
    });
    $productimg.appendTo($productbg);
    //商品价格
    var $price=$("<div></div>");
    $price.css({
        width:"114px",
        height:"16px",
        position:"absolute",
        top:"70%",
        left:"7%",
        "z-index":"10",
        "background-image":"url(imgs/shop/price.png)"
    });
    $price.appendTo($shopdiv);
    //购买按钮
    var $buttbuy=$("<div></div>");
    $buttbuy.css({
        width:"62px",
        height:"28px",
        position:"absolute",
        top:"81%",
        left:"3%",
        "z-index":"10",
        "background-image":"url(imgs/shop/buy.png)",
        cursor:"pointer"
    });
    $buttbuy.appendTo($shopdiv);
    //预览按钮
    var $buttpreview=$buttbuy.clone();
    $buttpreview.css({
        left:"51%",
        "background-image":"url(imgs/shop/look.png)"
    });
    $buttpreview.appendTo($shopdiv);

}