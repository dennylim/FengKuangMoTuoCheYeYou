/**
 * Created by Administrator on 2016/3/3.
 */
//车身类
function MotoBody(carsImg, maxSpeed){
    this.maxs = maxSpeed;
    var $cars=$("<div></div>");
    $cars.css({
        "background-image": "url("+carsImg+")",
        "background-position": "center",
        "background-repeat": "no-repeat",
        position: "absolute",
        top: "60px",
        left:"5px",
        "z-index": "10",
        width: "84px",
        height: "33px"
    });
    this.addTo=function($parent){
        $cars.appendTo($parent);
    }
}
//角色类
function Actor(riderimg,motobody,wheelimg){
    var self=this;
    var $me =$("<div></div>");
    $me.css({
        "width":"100px",
        "height":"120px",
        "position":"absolute",
        "top":"538px"
    });
    var $rider=$("<div></div>");
    $rider.attr("id","actor");
    $rider.css({
        "width":"72px",
        "height":"92px",
        "position":"absolute",
        "zIndex":"20",
        "backgroundImage":"url("+riderimg+")",
        "background-position":"center",
        "left":"10px"
    });
    $rider.appendTo($me);

    motobody.addTo($me);

    var $wheel1=$("<div></div>");
    $wheel1.attr("id","wheel1");
    $wheel1.css({
        "width":"35px",
        "height":"35px",
        "background-position":"center",
        "background-repeat": "no-repeat",
        "position":"absolute",
        "zIndex":"1",
        bottom: "0px",
        "backgroundImage":"url("+wheelimg+")",
        "top":"75px"

    });
    var $wheel2 = $wheel1.clone();
    $wheel2.css({"left":"62px"});
    $wheel1.appendTo($me);
    $wheel2.appendTo($me);

    this.wheelRoll=function(wheelV){//wheelV= 0，1,3,5,7(速度/加速度)
        var wheelDeg=0;
        setInterval(function (){
            $wheel1.css({"transform":"rotate("+wheelDeg+"deg)"});
            $wheel2.css({"transform":"rotate("+wheelDeg+"deg)"});
            wheelDeg+=wheelV;
            if(wheelDeg>360){
                wheelDeg=0;
            }
            if(wheelDeg%2==0){
                $rider.css({bottom:"19px"});
            }
            else{
                $rider.css({bottom:"20px"});
            }
        },1);
    };

    this.addTo=function($parent){
        $me.appendTo($parent);
        $me.css({left: "0px", top: "0px"});
    };
    this.setPosition=function(x,y){
        this.x=x;
        this.y=y;
        $me.css({left:x+"px",top:y+"px"});
    };

    this.getSpeed = function(){
        return motobody.maxs;
    };
    this.getNowSpeed=function(){
        return this.xx;
    };
    this.move=function(xx, yy){
        this.xx = xx;
        this.yy = yy;
        $me.css("left", parseInt($me.css("left"))+xx+"px");
        $me.css("top", parseInt($me.css("top"))+yy+"px");
    };
    this.getX = function(){
        return parseInt($me.css("left"));
    }
}